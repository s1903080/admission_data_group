### TO DO

1. To translate the failed csv file to normal csv file.
> Change the result of failed requests from API to correct latitude and longitude value.
   + 0_failed_data -> JIN
   + 1_failed_data -> Lambro
   + 2_failed_data -> SHENSHEN
   + 3_failed_data -> YU ZHAO


2. Upload the analysis result to everyone's workfolder.
> Everyone should write a description as detail as possible.
   1. Total amount of Applications for each year. `s1903080/data.md`
   2. Total amount of Offers for each year. `s1903080/data.md`
   3. Total amount of Accepts for each year. `s1903080/data.md`
   4. Total amount of UF for each year. `s1903080/data.md`
   5. Total amount of Entrants for each year. `s1903080/data.md`
   6. The number of apps for each year according to school and college. `s1903080/data.md`
   7. The number of entrants for each year according to school and college. `s1903080/data.md`
   8. The entrant/app rate for each year according to school and college. `s1903080/data.md`
   9. The number of entrants for each year according to school and Programme group. `s1903080/data.md`
   10. The Programme list of each year. `s1903080/data.md`
