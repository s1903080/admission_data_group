import React from 'react';
import {Switch, Route, BrowserRouter as Router} from 'react-router-dom';
import Home from './pages/HomePage/Homepage';
import Interactive from './pages/InteractivePage';
// import Globe from './components/Globe';
import style from './App.module.scss';
import './common.scss';
// import baseTexturePicA from './assets/a.jpg';
// import baseTexturePicB from './assets/b.jpg';
// import baseTexturePicC from './assets/c.jpg';
// import baseTexturePicD from './assets/d.jpg';
require('echarts');
require('echarts-gl');

function App() {
  return (
    <div className={style.App}>
      {/* <div className={style.row}>
        <div className={style.column}>
          <Globe id="A" texture={baseTexturePicA} />
        </div>
        <div className={style.column}>
          <Globe id="B" texture={baseTexturePicB} />
        </div>
      </div>
      <div className={style.row}>
        <div className={style.column}>
          <Globe id="c" texture={baseTexturePicC} />
        </div>
        <div className={style.column}>
          <Globe id="d" texture={baseTexturePicD} />
        </div>
      </div> */}
      <Router>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/interactive" exact component={Interactive} />
                {/* <Route path="/view" exact component={View}/>
                <Route path="/login" exact component={Login}/>
                <Route path="/main" exact component={Main}/>
                <Route path="/chart" exact component={Chart}/> */}

            </Switch>
      </Router>
    </div>
  );
}

export default App;
