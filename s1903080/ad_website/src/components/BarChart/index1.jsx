import React, { useEffect } from 'react';
import echarts from 'echarts';
import style from './style.module.scss';

export default function BarChart() {
    useEffect(() => {
        init()
    })
    function init() {
        const el = document.getElementById("barChart");
        const chartIns = echarts.init(el);
        var xAxisData = ['Africa', 'East Asia', 'Europe (Scotland)',
         'Europe (non UK)', 'Europe (rest of UK)', 'Latin America and the Caribbean',
        'Middle East', 'North America', 'South and Central Asia', 'Southeast Asia & Australia'];
        const data15 = [725, 63, 195, 277, 550, 88, 164, 268, 182, 260];
        const data16 = [1341,68,248,346,690,93,181,358,299,292];
        const data17 = [2798,106,249,432,805,149,214,420,587,319];
        const data18 = [1506,119,268,397,802,129,208,416,304,289];
        
        const option = {
            title: {
                // text: 'Reigion Distribution'
            },
            legend: {
                data: ['2015', '2016', '2017', '2018'],
                align: 'left'
            },
            toolbox: {
                // y: 'bottom',
                feature: {
                    magicType: {
                        type: ['stack', 'tiled']
                    },
                    dataView: {},
                    saveAsImage: {
                        pixelRatio: 2
                    }
                }
            },
            tooltip: {},
            xAxis: {
                data: xAxisData,
                silent: false,
                axisLabel: {
                    fontSize: 12,
                    rotate: 10,
                    margin: 18
                },
                boundaryGap: true,
                splitLine: {
                    show: false
                }
            },
            yAxis: {
            },
            series: [{
                name: '2015',
                type: 'bar',
                data: data15,
                itemStyle: {
                    color: '#1F1271'
                },
                animationDelay: function (idx) {
                    return idx * 10;
                }
            }, {
                name: '2016',
                type: 'bar',
                data: data16,
                itemStyle: {
                    color: '#3839A3'
                },
                animationDelay: function (idx) {
                    return idx * 10 + 100;
                }
            }, {
                name: '2017',
                type: 'bar',
                data: data17,
                itemStyle: {
                    color: '#406CF4'
                },
                animationDelay: function (idx) {
                    return idx * 10 + 100;
                }
            }, {
                name: '2018',
                type: 'bar',
                data: data18,
                itemStyle: {
                    color: '#02E5FE'
                },
                animationDelay: function (idx) {
                    return idx * 10 + 100;
                }
            }],
            animationEasing: 'elasticOut',
            animationDelayUpdate: function (idx) {
                return idx * 5;
            }
        };
        chartIns.setOption(option, true);
    }
    return (
        <div className={style.bar} id="barChart">
            8989
        </div>
    )
}