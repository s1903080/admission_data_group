import React, { useEffect } from 'react';
import echarts from 'echarts';
import mapData from 'echarts/map/json/world.json';
import data from '../../data/lat_lng';
import texture from '../../assets/canyon.hdr';
import style from './style.module.scss';

export default function GlobBarChart(props) {
    const { selectedIndex, max } = props;
    useEffect(() => {
        init()
    })
    function init() {
        const el = document.getElementById("barGlobChart");
        const chartIns = echarts.init(el);
        echarts.registerMap('world',require('echarts/map/json/world.json'));
        const dataN = data.APP_YEAR[selectedIndex];
        // const dataN = [[12.83333333, 42.83333333, 10],
        // [54.0, 24.0, 44],
        // [33.0, 35.0, 10],
        // [-97.0, 38.0, 195],
        // [138.0, 36.0, 9],
        // [6.16666666, 49.75, 2],
        // [5.75, 52.5, 16],
        // [77.0, 20.0, 75],
        // [133.0, -27.0, 141],
        // [103.8, 1.36666666, 27],
        // [8.0, 47.0, 21],
        // [112.5, 2.5, 27],
        // [-88.75, 17.25, 4],
        // [38.0, 1.0, 65],
        // [70.0, 30.0, 71],
        // [105.0, 46.0, 1],
        // [122.0, 13.0, 5],
        // [-8.0, 53.0, 45],
        // [-4.0, 40.0, 12],
        // [105.0, 35.0, 21],
        // [13.33333333, 47.33333333, 4],
        // [4.0, 50.83333333, 10],
        // [9.0, 51.0, 28],
        // [45.75, 29.5, 9],
        // [51.25, 25.5, 16],
        // [-2.0, 8.0, 69],
        // [-11.5, 8.5, 6],
        // [32.0, 1.0, 95],
        // [15.5, 49.75, 1],
        // [-80.0, 9.0, 2],
        // [175.0, -18.0, 2],
        // [-77.5, 18.25, 12],
        // [8.0, 10.0, 134],
        // [24.0, -29.0, 36],
        // [114.16666666, 22.25, 21],
        // [-4.5, 54.25, 3],
        // [17.0, -22.0, 5],
        // [45.0, 25.0, 36],
        // [30.0, -20.0, 41],
        // [30.0, 7.0, 3],
        // [-4.0, 17.0, 1],
        // [30.0, -2.0, 20],
        // [-95.0, 60.0, 73],
        // [100.0, 15.0, 6],
        // [113.55, 22.16666666, 3],
        // [47.5, 40.5, 1],
        // [-76.0, 24.25, 6],
        // [120.0, -5.0, 5],
        // [15.0, 62.0, 13],
        // [17.0, 25.0, 2],
        // [34.0, -13.5, 48],
        // [22.0, 39.0, 25],
        // [14.58333333, 35.83333333, 21],
        // [65.0, 33.0, 6],
        // [90.0, 24.0, 14],
        // [147.0, -6.0, 6],
        // [-5.35, 36.13333333, 2],
        // [24.0, 56.0, 1],
        // [57.0, 21.0, 8],
        // [121.0, 23.5, 6],
        // [68.0, 48.0, 2],
        // [30.0, -15.0, 31],
        // [38.0, 8.0, 29],
        // [-5.0, 8.0, 1],
        // [9.0, 34.0, 1],
        // [35.0, 39.0, 9],
        // [30.0, 27.0, 24],
        // [167.0, -16.0, 1],
        // [-5.0, 32.0, 2],
        // [12.0, 6.0, 8],
        // [50.55, 26.0, 10],
        // [57.55, -20.28333333, 11],
        // [2.0, 46.0, 11],
        // [26.0, 64.0, 2],
        // [-8.0, 39.5, 3],
        // [174.0, -41.0, 18],
        // [47.0, -20.0, 2],
        // [-80.5, 19.5, 3],
        // [84.0, 28.0, 5],
        // [-102.0, 23.0, 7],
        // [10.0, 56.0, 5],
        // [36.0, 31.0, 4],
        // [48.0, 15.0, 2],
        // [30.0, 15.0, 17],
        // [10.0, 62.0, 4],
        // [20.0, 52.0, 3],
        // [24.0, -22.0, 14],
        // [44.0, 33.0, 3],
        // [11.75, -1.0, 1],
        // [25.0, 57.0, 1],
        // [18.5, -12.5, 2],
        // [39.0, 15.0, 2],
        // [35.0, -18.25, 5],
        // [32.0, 49.0, 5],
        // [-77.5, -2.0, 4],
        // [34.75, 31.5, 1],
        // [-16.56666666, 13.46666666, 4],
        // [-2.0, 13.0, 1],
        // [-61.33333333, 15.41666666, 1],
        // [98.0, 22.0, 10],
        // [35.83333333, 33.83333333, 3],
        // [-59.0, 5.0, 4],
        // [-2.16666666, 49.25, 1],
        // [-2.58333333, 49.46666666, 2],
        // [-90.25, 15.5, 2],
        // [25.0, 46.0, 2],
        // [81.0, 7.0, 5],
        // [-172.33333333, -13.58333333, 2],
        // [-9.5, 6.5, 2],
        // [20.0, 47.0, 2],
        // [159.0, -8.0, 1],
        // [1.5, 42.5, 1],
        // [-72.41666666, 19.0, 1],
        // [-55.0, -10.0, 3],
        // [55.66666666, -4.58333333, 1],
        // [-72.0, 4.0, 1],
        // [73.0, 3.25, 1],
        // [28.5, -29.5, 2],
        // [31.5, -26.5, 2],
        // [20.0, 41.0, 1],
        // [-18.0, 65.0, 2],
        // [15.5, 45.16666666, 2],
        // [26.0, 59.0, 1],
        // [125.91666666, -8.83333333, 1],
        // [21.0, 44.0, 1],
        // [-64.75, 32.33333333, 2],
        // [19.3, 42.5, 1],
        // [-59.53333333, 13.16666666, 2],
        // [49.0, 10.0, 2],
        // [-76.0, -10.0, 1],
        // [71.0, 39.0, 1],
        // [105.0, 13.0, 1]];
        chartIns.setOption({
            // backgroundColor: '#170F65',
            geo3D: {
                map: 'world',
                shading: 'lambert',
                light: {
                    main: {
                        intensity: 8,
                        shadow: false,
                        shadowQuality: 'high',
                        alpha: 50
                    },
                    ambient: {
                        intensity: 0
                    },
                    ambientCubemap: {
                        // texture: texture,
                        exposure: 1,
                        diffuseIntensity: 0.5
                    }
                },
                viewControl: {
                    distance: 80,
                    rotateMouseButton: 'left',
                    // panMouseButton: 'left',
                    // autoRotateAfterStill: 3,
                    // autoRotate: true,
                    maxDistance: 1000,
                    panSensitivity: 1,
                    minDistance: 10,
                    
                },

                groundPlane: {
                    show: false,
                    color: '#ffffff'
                },
                postEffect: {
                    enable: false,
                    bloom: {
                        enable: false
                    },
                    SSAO: {
                        radius: 1,
                        intensity: 1,
                        enable: true
                    },
                    depthOfField: {
                        enable: false,
                        focalRange: 10,
                        blurRadius: 10,
                        fstop: 1
                    }
                },
                temporalSuperSampling: {
                    enable: true
                },
                itemStyle: {
                    color: "#ffffff",
                    borderWidth: 1,
                    borderColor: "#170F65"
                },
                emphasis: {
                    itemStyle: {
                        color: "#170F65",
                        borderWidth: 1,
                        borderColor: "#ffffff"
                    }
                },
                regionHeight: 0.1,
                label: {
                    show: false,
                    formatter: (i) => {
                        return i.name
                    },
                    textStyle: {
                        color: "#170F65",
                        borderWidth: 1,
                        borderColor: "#170F65",
                        fontFamily: 'Dosis',
                        fontSize: 18
                    }
                }
            },
            // visualMap: {
            //     max: 2000,
            //     calculable: true,
            //     realtime: true,
            //     inRange: {
            //         color: ['#152273', '#04D2F0']
            //     },
            //     outOfRange: {
            //         colorAlpha: 0
            //     }
            // },
            // visualMap: {
            //     max: max,
            //     calculable: true,
            //     realtime: false,
            //     inRange: {
            //         // colorLightness: [0.2, 0.9]
            //     },
            //     textStyle: {
            //         color: '#000'
            //     },
            //     controller: {
            //         inRange: {
            //             color: '#6543ff'
            //         }
            //     },
            //     outOfRange: {
            //         colorAlpha: 0
            //     }
            // },
            series: [{
                type: 'bar3D',
                coordinateSystem: 'geo3D',
                shading: 'lambert',
                data: dataN,
                barSize: 0.5,
                minHeight: 1,
                silent: true,
                itemStyle: {
                    color: '#170F65'
                },
                label: {
                    show: true,
                    distance: 1,
                    formatter: (obj) => {
                        return obj.data[2]
                    },
                    textStyle: {
                        color: "#170F65",
                        borderWidth: 1,
                        borderColor: "#170F65",
                        fontFamily: 'Dosis',
                        fontSize: 18
                    }
                }
            }]
        });

    }
    return (
        <div className={style.bar} id="barGlobChart">
            
        </div>
    )
}