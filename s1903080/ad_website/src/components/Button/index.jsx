import React from 'react';
import style from './style.module.scss';

export default function Button(props) {
    const { type } = props
    return (
        <button 
            className={type == "primary" ? style.primary : style.blank}
            {...props}
        >
            {props.children}
        </button>
    )
}