import React from 'react';
import CountUp from 'react-countup';
import style from './style.module.scss';

export default function GenderChart(props) {
    const { malePercent, femalePercent } = props;
    return (
        <React.Fragment>
            <div className={style.wrapper}>
                <div className={style.overview}>
                    <p><span>Female: </span> <CountUp end={femalePercent} suffix="%" style={{ fontSize: '48px' }}/></p>
                    <p className={style.right}><span>Male: </span> <CountUp end={malePercent} suffix="%" style={{ fontSize: '48px' }}/></p>
                </div>
                <div className={style.processBar}>
                    <div className={style.female} style={{ width: femalePercent + '%' }}>

                    </div>
                    <div className={style.male} style={{ width: malePercent + '%' }}>

                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}