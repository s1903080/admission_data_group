import React, { useEffect, useState } from 'react';
import echarts from 'echarts';
import data from '../../data/lat_lng';
// import baseTexturePic from './assets/world.topo.bathy.200401.jpg';
// import baseTexturePic from './assets/c.jpg';
import heightTexturePic from './assets/bathymetry_bw_composite_4k.jpg';
import style from './style.module.scss';

export default function Globe(props) {
    const [distance, setDistance] = useState(350);
    // window.addEventListener("resize", () => {
    //     generateGlob()
    // })
    useEffect(() => {
        generateGlob()
    })
    function generateGlob() {
        const el = document.getElementById(props.id);
        // el.style.width = window.innerWidth + "px";
        // el.style.height = window.innerHeight + "px";
        var chart = echarts.init(el);
        const data2Ed = data.REGION_APP["2018"].map((item) => {
            const [origin, source] = item
            return [source, origin]
        })
        chart.setOption({
            globe: {
                baseTexture: props.texture,
                heightTexture: heightTexturePic,
                width: window.innerWidth/1.2,
                height: window.innerHeight/1.2,
                shading: 'lambert',
                left: -280,
                top: 210,

                globRadius: 20,
                light: {
                    ambient: {
                        intensity: 0.8
                    },
                    main: {
                        intensity: 0.8
                    }
                },
    
                viewControl: {
                    autoRotate: true,
                    distance: distance,
                    alpha: 0.6,
                    maxDistance: 400,
                    minDistance: 280
                }
            },
            series: {
    
                type: 'lines3D',
    
                coordinateSystem: 'globe',
    
                blendMode: 'lighter',
    
                lineStyle: {
                    width: 1,
                    // color: 'rgb(50, 50, 150)',
                    color: 'rgb(23, 194, 249)',
                    opacity: 0.6
                },
                
                effect: {
                    show: true,
                    trailWidth: 2,
                    trailLength: 0.15,
                    trailOpacity: 0.8,
                    trailColor: 'rgb(30, 30, 60)'
                },
                // polyline: true,
    
                data: data2Ed
            }
        });
    }
    return(
        <React.Fragment>
            <div className={style.glob} id={props.id}>

            </div>
        </React.Fragment>
    )
}
