import React, { useEffect } from 'react';
import echarts from 'echarts';
import style from './style.module.scss';
import data from '../../data/lat_lng';


export default function LayerChart(props) {
    // window.addEventListener("resize", () => {
    //     generateChart()
    // })
    useEffect(() => {
        generateChart()
    })
    const { id } = props;
    function generateChart() {
        const el = document.getElementById(id);
        var chart = echarts.init(el);
        const dataSet = data.AGE_GENDER[id]
        const age = ['Under 25', '25-34', '35-44', '45-54', '55+'];
        const res = {}
        age.map((item, index) => {
            res[item] = dataSet[index]
        })
        const color = ["#8d5cff", "#3839A3", "#406CF4", "#1F1271", "#02E5FE"];
        
        const option = {
            title: {
                // text: '漏斗图',
                // subtext: '纯属虚构'
            },
            backgroundColor: "#000000",
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c}"
            },
            legend: {
                data: ['Under 25', '25-34', '35-44', '45-54', '55+'],

            },
            series: [
                {
                    name: 'male',
                    type: 'funnel',
                    left: '20%',
                    width: '60%',
                    sort: "none",
                    label: {
                        normal: {
                            formatter: '{b}'
                        },
                        emphasis: {
                            position:'inside',
                            formatter: '{b}: {c}'
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    itemStyle: {
                        normal: {
                            // opacity: 0.7
                        },
                    },
                    data: age.map((item, index) => {
                        return { value: res[item][1], name: item, itemStyle: { color: color[index] } }
                    })
                },
                {
                    name: 'female',
                    type: 'funnel',
                    left: '20%',
                    width: '60%',
                    maxSize: '80%',
                    sort: "none",
                    label: {
                        normal: {
                            position: 'inside',
                            formatter: '{c}',
                            textStyle: {
                                color: '#fff'
                            }
                        },
                        emphasis: {
                            position:'inside',
                            formatter: '{b}: {c}'
                        }
                    },
                    itemStyle: {
                        normal: {
                            // opacity: 0.5,
                            borderColor: '#fff',
                            borderWidth: 1
                        }
                    },
                    data: age.map((item, index) => {
                        return { value: res[item][0], name: item, itemStyle: { color: color[index] } }
                    })
                }
            ]
        };
        chart.setOption(option, true);
    }
    return(
        <React.Fragment>
            <div
             className={style.LayerChart} 
             id={id}>

            </div>
        </React.Fragment>
    )
}
