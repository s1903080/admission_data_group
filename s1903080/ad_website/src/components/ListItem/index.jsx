import React from 'react';
import CountUp from 'react-countup';
import style from './style.module.scss';
import p1 from './assets/1.png';
import p2 from './assets/2.png';
import p3 from './assets/3.png';
import p4 from './assets/4.png';
import p5 from './assets/5.png';
import p6 from './assets/6.png';
import p7 from './assets/7.png';
import p8 from './assets/8.png';
import p9 from './assets/9.png';
import p10 from './assets/10.png';

import a1 from './assets/2015/1.svg';
import a2 from './assets/2015/2.svg';
import a3 from './assets/2015/3.svg';
import a4 from './assets/2015/4.svg';
import a5 from './assets/2015/5.svg';
import a6 from './assets/2015/6.svg';
import a7 from './assets/2015/7.svg';
import a8 from './assets/2015/8.svg';
import a9 from './assets/2015/9.svg';
import a10 from './assets/2015/10.svg';

import b1 from './assets/2016/1.svg';
import b2 from './assets/2016/2.svg';
import b3 from './assets/2016/3.svg';
import b4 from './assets/2016/4.svg';
import b5 from './assets/2016/5.svg';
import b6 from './assets/2016/6.svg';
import b7 from './assets/2016/7.svg';
import b8 from './assets/2016/8.svg';
import b9 from './assets/2016/9.svg';
import b10 from './assets/2016/10.svg';

import c1 from './assets/2017/1.svg';
import c2 from './assets/2017/2.svg';
import c3 from './assets/2017/3.svg';
import c4 from './assets/2017/4.svg';
import c5 from './assets/2017/5.svg';
import c6 from './assets/2017/6.svg';
import c7 from './assets/2017/7.svg';
import c8 from './assets/2017/8.svg';
import c9 from './assets/2017/9.svg';
import c10 from './assets/2017/10.svg';

import d1 from './assets/2018/1.svg';
import d2 from './assets/2018/2.svg';
import d3 from './assets/2018/3.svg';
import d4 from './assets/2018/4.svg';
import d5 from './assets/2018/5.svg';
import d6 from './assets/2018/6.svg';
import d7 from './assets/2018/7.svg';
import d8 from './assets/2018/8.svg';
import d9 from './assets/2018/9.svg';
import d10 from './assets/2018/10.svg';

import e_a1 from './assets/entrants/2015/1.svg';
import e_a2 from './assets/entrants/2015/2.svg';
import e_a3 from './assets/entrants/2015/3.svg';
import e_a4 from './assets/entrants/2015/4.svg';
import e_a5 from './assets/entrants/2015/5.svg';
import e_a6 from './assets/entrants/2015/6.svg';
import e_a7 from './assets/entrants/2015/7.svg';
import e_a8 from './assets/entrants/2015/8.svg';
import e_a9 from './assets/entrants/2015/9.svg';
import e_a10 from './assets/entrants/2015/10.svg';

import e_b1 from './assets/entrants/2016/1.svg';
import e_b2 from './assets/entrants/2016/2.svg';
import e_b3 from './assets/entrants/2016/3.svg';
import e_b4 from './assets/entrants/2016/4.svg';
import e_b5 from './assets/entrants/2016/5.svg';
import e_b6 from './assets/entrants/2016/6.svg';
import e_b7 from './assets/entrants/2016/7.svg';
import e_b8 from './assets/entrants/2016/8.svg';
import e_b9 from './assets/entrants/2016/9.svg';
import e_b10 from './assets/entrants/2016/10.svg';

import e_c1 from './assets/entrants/2017/1.svg';
import e_c2 from './assets/entrants/2017/2.svg';
import e_c3 from './assets/entrants/2017/3.svg';
import e_c4 from './assets/entrants/2017/4.svg';
import e_c5 from './assets/entrants/2017/5.svg';
import e_c6 from './assets/entrants/2017/6.svg';
import e_c7 from './assets/entrants/2017/7.svg';
import e_c8 from './assets/entrants/2017/8.svg';
import e_c9 from './assets/entrants/2017/9.svg';
import e_c10 from './assets/entrants/2017/10.svg';

import e_d1 from './assets/entrants/2018/1.svg';
import e_d2 from './assets/entrants/2018/2.svg';
import e_d3 from './assets/entrants/2018/3.svg';
import e_d4 from './assets/entrants/2018/4.svg';
import e_d5 from './assets/entrants/2018/5.svg';
import e_d6 from './assets/entrants/2018/6.svg';
import e_d7 from './assets/entrants/2018/7.svg';
import e_d8 from './assets/entrants/2018/8.svg';
import e_d9 from './assets/entrants/2018/9.svg';
import e_d10 from './assets/entrants/2018/10.svg';




export default function ListItem(props) {
    const { rank, name, value, selectedYear, nation, selectedProcess } = props;
    const rankList = [p1, p2, p3, p4, p5, p6, p7, p8, p9, p10];
    const flagListApps = {
        "2015": [
            a1,a2,a3,a4,a5,a6,a7,a8,a9,a10
        ],
        "2016": [
            b1,b2,b3,b4,b5,b6,b7,b8,b9,b10
        ],
        "2017": [
            c1,c2,c3,c4,c5,c6,c7,c8,c9,c10
        ],
        "2018": [
            d1,d2,d3,d4,d5,d6,d7,d8,d9,d10
        ],
    }
    const flagListEntrants = {
        "2015": [
            e_a1,e_a2,e_a3,e_a4,e_a5,e_a6,e_a7,e_a8,e_a9,e_a10
        ],
        "2016": [
            e_b1,e_b2,e_b3,e_b4,e_b5,e_b6,e_b7,e_b8,e_b9,e_b10
        ],
        "2017": [
            e_c1,e_c2,e_c3,e_c4,e_c5,e_c6,e_c7,e_c8,e_c9,e_c10
        ],
        "2018": [
            e_d1,e_d2,e_d3,e_d4,e_d5,e_d6,e_d7,e_d8,e_d9,e_d10
        ],
    }
    let flagList = {}
    switch (selectedProcess) {
        case 0:
            flagList = flagListApps;
            break;
        case 1:
            flagList = flagListEntrants
            break;
        default:
            break;
    }
    return (
        <div className={style.ListItem}>
            <img src={rankList[rank - 1]} alt="" className={rank > 3 ? style.rankSmall : style.rank}/>
            {
                nation ? <img src={flagList[selectedYear][(rank - 1)]} alt="" className={style.flag}/> : ''
            }
            <span className={style.name}>{name}</span>
            <span className={rank <= 3 ? style.value : style.valueSmall}><CountUp end={value}/></span>
        </div>
    )
}