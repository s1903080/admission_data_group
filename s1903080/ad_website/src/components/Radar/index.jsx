import React, { useEffect } from 'react';
import echarts, { init } from 'echarts';
import style from './style.module.scss';

export default function Radar(props) {
    useEffect(() => {
        init()
    })
    function init() {
        const dom = document.getElementById("radar");
        const radarChart = echarts.init(dom);
        let option = null;
        // Schema:
    // date,AQIindex,PM2.5,PM10,CO,NO2,SO2
    // var data15 = [
    //     [725,63,195,277,550,88,164,268,182,260,1]
    // ];
    
    // var data16 = [
    //     [1341,68,248,346,690,93,181,358,299,292,1],
    // ];
    
    // var data17 = [
    //     [2798,106,249,432,805,149,214,420,587,319,1]
    // ];

    // var data18 = [
    //     [1506,119,268,397,802,129,208,416,304,289,1]
    // ];

    var data15=[
        [0.64,0.7,0.82,0.84,0.55,0.35,1]
        ];
    
    var data16=[
        [0.58,0.63,0.78,0.81,0.53,0.31,2]
        ];
    
    var data17=[
        [0.54,0.54,0.8,0.68,0.43,0.24,3]
        ];
    
    var data18=[
        [0.6,0.67,0.82,0.81,0.55,0.33,4]
        ];
    
    var lineStyle = {
        normal: {
            width: 1,
            opacity: 0.5
        }
    };
    
    option = {
        backgroundColor: '#ffffff',
        title: {
            // text: 'AQI - 雷达图',
            // left: 'center',
            // textStyle: {
            //     color: '#eee'
            // }
        },
        tooltip: {},
        legend: {
            bottom: 5,
            data: ['2015/6', '2016/7', '2017/8', '2018/9'],
            itemGap: 20,
            textStyle: {
                color: '#170F65',
                fontSize: 14,
                fontFamily: 'Dosis'
            },
            selectedMode: 'multiple'
        },
        // visualMap: {
        //     show: true,
        //     min: 0,
        //     max: 20,
        //     dimension: 6,
        //     inRange: {
        //         colorLightness: [0.5, 0.8]
        //     }
        // },
        radar: {
            indicator: [
                // {name: 'Africa', max: 3000},
                // {name: 'East Asia', max: 200},
                // {name: 'Europe (Scotland)', max: 300},
                // {name: 'Europe (non UK)', max: 500},
                // {name: 'Europe (rest of UK)', max: 1000},
                // {name: 'Latin America and the Caribbean', max: 200},
                // {name: 'Middle East', max: 250},
                // {name: 'North America', max: 500},
                // {name: 'South and Central Asia', max: 600},
                // {name: 'Southeast Asia & Australia', max: 400}
                // {name: 'Africa'},
                // {name: 'East Asia'},
                // {name: 'Europe (Scotland)'},
                // {name: 'Europe (non UK)'},
                // {name: 'Europe (rest of UK)'},
                // {name: 'Latin America and the Caribbean'},
                // {name: 'Middle East'},
                // {name: 'North America'},
                // {name: 'South and Central Asia'},
                // {name: 'Southeast Asia & Australia'}
                {name:"Offers/Apps",max:1},
                {name:"UF/Offers",max:1},
                {name:"Accepts/Offers",max:1},
                {name:"UF/Accepts",max:1},
                {name:"Entrants/Offers",max:1},
                {name:"Entrants/Apps",max:1}
            ],
            shape: 'circle',
            splitNumber: 5,
            name: {
                textStyle: {
                    color: '#170F65',
                    fontFamily: 'Dosis',
                    fontSize: 18
                }
            },
            splitLine: {
                lineStyle: {
                    color: [
                        'rgba(23, 15, 101, 0.1)', 'rgba(23, 15, 101, 0.2)',
                        'rgba(23, 15, 101, 0.4)', 'rgba(23, 15, 101, 0.6)',
                        'rgba(23, 15, 101, 0.8)', 'rgba(23, 15, 101, 1)'
                    ].reverse()
                }
            },
            splitArea: {
                show: false
            },
            axisLine: {
                lineStyle: {
                    color: 'rgba(23, 15, 101, 0.5)'
                }
            }
        },
        series: [
            {
                name: '2015/6',
                type: 'radar',
                lineStyle: lineStyle,
                data: data15,
                symbol: 'none',
                itemStyle: {
                    normal: {
                        color: '#02E5FE'
                    }
                },
                areaStyle: {
                    normal: {
                        opacity: 0.4
                    }
                }
            },
            {
                name: '2016/7',
                type: 'radar',
                lineStyle: lineStyle,
                data: data16,
                symbol: 'none',
                itemStyle: {
                    normal: {
                        color: '#3839A3'
                    }
                },
                areaStyle: {
                    normal: {
                        opacity: 0.4
                    }
                }
            },
            {
                name: '2017/8',
                type: 'radar',
                lineStyle: lineStyle,
                data: data17,
                symbol: 'none',
                itemStyle: {
                    normal: {
                        color: '#406CF4'
                    }
                },
                areaStyle: {
                    normal: {
                        opacity: 0.4
                    }
                }
            },
            {
                name: '2018/9',
                type: 'radar',
                lineStyle: lineStyle,
                data: data18,
                symbol: 'none',
                itemStyle: {
                    normal: {
                        color: '#1F1271'
                    }
                },
                areaStyle: {
                    normal: {
                        opacity: 0.4
                    }
                }
            }
        ]
    };
    radarChart.setOption(option, true);
    }
    return(
        <React.Fragment>
            <div className={style.Radar} id="radar">

            </div>
        </React.Fragment>
    )
}
