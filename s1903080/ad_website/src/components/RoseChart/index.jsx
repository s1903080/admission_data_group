import React, { useEffect } from 'react';
import echarts from 'echarts';
import style from './style.module.scss';
import data from '../../data/lat_lng';


export default function RoseChart(props) {
    // window.addEventListener("resize", () => {
    //     generateChart()
    // })
    useEffect(() => {
        generateChart()
    })
    const { id } = props;
    function generateChart() {
        const el = document.getElementById(id);
        var chart = echarts.init(el);
        const dataSet = data.AGE_GENDER[id]
        const age = ['Under 25', '25-34', '35-44', '45-54', '55+'];
        const res = {}
        age.map((item, index) => {
            res[item] = dataSet[index]
        })
        const color = ["#8d5cff", "#3839A3", "#406CF4", "#1F1271", "#02E5FE"];
        const dataResFemale = Object.keys(res).map((item, index) => {
            return {
                name: item,
                value: res[item][0],
                itemStyle: {
                    color: `rgba(7, 199, 238, ${1 - 0.1 * (index)})`
                    // color: `rgba(23, 15, 101, ${1 - 0.1 * (index)})`

                }
            }
        })
        const dataResMale = Object.keys(res).map((item, index) => {
            return {
                name: item,
                value: res[item][1],
                itemStyle: {
                    // color: `rgba(23, 15, 101, ${1 - 0.1 * (index)})`
                    color: `rgba(23, 15, 101, ${1 - 0.1 * (index)})`
                }
            }
        })
        const option = {
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                x : 'center',
                y : 'bottom',
                data: age.map((item) => {
                    return {
                        name: item,
                        icon: 'path://M149,123 C160.59798,123 170,113.59798 170,102 C170,90.4020203 160.59798,81 149,81 C137.40202,81 128,90.4020203 128,102'
                    }
                }),
                
            },
            hoverOffset: 1,
            calculable : true,
            series : [
                {
                    name:'female',
                    type:'pie',
                    radius : [30, 130],
                    center : ['25%', '50%'],
                    roseType : 'radius',
                    label: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: false
                        },
                        position: 'center'
                    },
                    lableLine: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: true
                        }
                    },
                    data: dataResFemale,
                },
                {
                    name:'male',
                    type:'pie',
                    radius : [30, 130],
                    center : ['75%', '50%'],
                    roseType : 'radius',
                    label: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: false
                        }
                    },
                    lableLine: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: true
                        }
                    },
                    data: dataResMale
                }
            ]
        };
        
        
        chart.setOption(option, true);
    }
    return(
        <React.Fragment>
            <div
             className={style.RoseChart} 
             id={id}>

            </div>
        </React.Fragment>
    )
}
