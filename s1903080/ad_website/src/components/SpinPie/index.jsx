$(function () {
    var t = 500,
        r = 500,
        a = 20,
        n = 20,
        e = function (t, r) {
            return Math.floor(Math.random() * (r - t)) + t
        },
        i = function (t, r, a) {
            for (var n = [], i = 0; t > i; i++) n.push({
                inner: r,
                outer: e(a - 60, a)
            })
            return n
        },
        o = e(0, 3)
    if (0 == o) var u = d3.scale.category20()
    else if (1 == o) var u = d3.scale.category20b()
    else if (2 == o) var u = d3.scale.category20c()
    var s = i(n, a, Math.min(t, r) / 2),
        c = d3.select("#canvas").append("svg").attr("width", t).attr("height", r),
        l = d3.svg.arc().innerRadius(function (t) {
            return t.data.inner
        }).outerRadius(function (t) {
            return t.data.outer
        }),
        d = d3.layout.pie().value(function (t) {
            return e(10, 15)
        }).sort(null),
        f = d3.svg.arc().innerRadius(function (t) {
            return console.log(t), t.data.inner
        }).outerRadius(function (a) {
            return e(100, Math.min(t, r) / 2)
        }),
        v = function () {
            var a = e(0, 360),
                n = e(0, 7),
                i = "cubic-in-out"
            0 == n ? i = "linear" : 1 == n ? i = "quad" : 2 == n ? i = "back" : 3 == n ? i = "elastic" : 4 == n && (i = "bounce"), c.selectAll("path").transition().attr("d", function (t) {
                return f(t)
            }).attr("transform", "translate(" + t / 2 + "," + r / 2 + ") rotate(" + a + ",0,0)").duration(500).ease(i)
        }
    c.selectAll("path").data(d(s)).enter().append("path").attr("d", function (t) {
        return l(t)
    }).attr("stroke-width", 2).attr("stroke", "black").style("stroke-opacity", .3).style("stroke-linejoin", "round").attr("fill", function (t, r) {
        return u(r)
    }).style("opacity", .9).attr("class", "pie").attr("transform", "translate(250, 250)"), setInterval(v, 1e3)
})