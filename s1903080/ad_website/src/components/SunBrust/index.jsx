import React, { useEffect } from 'react';
import echarts from 'echarts';
import data from '../../data/school_college';
import style from './style.module.scss';

export default function GlobBarChart(props) {
    useEffect(() => {
        init()
    })
    function init() {
        const el = document.getElementById("sun");
        const chartIns = echarts.init(el);
        const dataSet = Object.keys(data.SCHOOL_COLLAGE["2015"]).map((item) => {
            const schools = Object.keys(data.SCHOOL_COLLAGE["2015"][item]);
            return {
                name: item,
                children: schools.map((i) => {
                    const school = data.SCHOOL_COLLAGE["2015"][item][i];
                    return {
                        "name": i,
                        "value": school,
                        "label": {
                            "color": "#fff000"
                        }
                    }
                })
            }
        });
        const option = {
            series: {
                type: 'sunburst',
                // highlightPolicy: 'ancestor',
                data: dataSet,
                radius: [0, '90%'],
                label: {
                    rotate: 'radial'
                }
            }
        };
        chartIns.setOption(option, true);
        el.addEventListener("mouseover", (e) => {
            console.log(e)
        })
    }
    return (
        <div className={style.sun} id="sun">
            
        </div>
    )
}