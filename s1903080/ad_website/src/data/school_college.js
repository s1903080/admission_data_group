const SCHOOL_COLLAGE = {
    "2015": {
        "HSS": {
            "Law": 322,
            "ECA": 38,
            "HCA": 115,
            "PPLS": 72,
            "LLC": 64,
            "Health": 111,
            "SPS": 77,
            "Education": 167
        },
        "MVM": {
            "Dick Vet": 180,
            "Medical Sch": 104,
            "Biomed Sci": 376,
            "Clinical Sci": 672,
            "Molecular Sci": 357
        },
        "SCE": {
            "Chemistry": 7,
            "Biology": 42,
            "SCE": 23,
            "Geosciences": 45
        }
    },
    "2016": {
        "HSS": {
            "Law": 395,
            "ECA": 64,
            "HCA": 99,
            "PPLS": 73,
            "LLC": 98,
            "Health": 107,
            "SPS": 171,
            "Education": 227
        },
        "MVM": {
            "Dick Vet": 343,
            "Pop Health Sci": 1,
            "Medical Sch": 134,
            "Biomed Sci": 501,
            "Clinical Sci": 762,
            "Molecular Sci": 650
        },
        "SCE": {
            "Chemistry": 22,
            "Biology": 59,
            "SCE": 74,
            "Geosciences": 136
        }
    },
    "2017": {
        "HSS": {
            "Law": 463,
            "Business": 63,
            "ECA": 92,
            "HCA": 129,
            "PPLS": 100,
            "LLC": 64,
            "Health": 130,
            "SPS": 244,
            "Education": 144
        },
        "MVM": {
            "Dick Vet": 397,
            "Medical Sch": 271,
            "Biomed Sci": 745,
            "Clinical Sci": 971,
            "Molecular Sci": 1760
        },
        "SCE": {
            "Chemistry": 3,
            "Biology": 51,
            "SCE": 183,
            "Geosciences": 269
        }
    },
    "2018": {
        "HSS": {
            "Law": 409,
            "Business": 7,
            "ECA": 75,
            "HCA": 109,
            "PPLS": 122,
            "LLC": 2,
            "Health": 118,
            "SPS": 373,
            "Education": 133
        },
        "MVM": {
            "Dick Vet": 471,
            "Medical Sch": 172,
            "Biomed Sci": 469,
            "Clinical Sci": 767,
            "Molecular Sci": 593
        },
        "SCE": {
            "Biology": 39,
            "SCE": 178,
            "Geosciences": 401
        }
    }
};

export default {
    SCHOOL_COLLAGE
}