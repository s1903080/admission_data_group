import React, { useState } from 'react';
import CountUp from 'react-countup';
import Globe from '../../components/Globe';
import style from './style.module.scss';
import baseTexturePicE from '../../assets/e.png';
import baseTexturePicA from '../../assets/a.jpg';
import baseTexturePicB from '../../assets/b.jpg';
import baseTexturePicC from '../../assets/c.jpg';
import baseTexturePicD from '../../assets/d.jpg';
import homepage_img from './assets/ad_2.png';
import Button from '../../components/Button';
import Radar from '../../components/Radar';
import BarChart from '../../components/BarChart';
import BigGlobe from '../../components/BigGlobe';
import GlobBarChart from '../../components/BarGlobe';
import bgImg from '../../assets/fg.png';
import data from '../../data/lat_lng';
import genderData from '../../data/gender_app';
import LayerChart from '../../components/LayerChart';
import RoseChart from '../../components/RoseChart';
import SunBrust from '../../components/SunBrust';
import up from '../../assets/up.png';
import male from '../../assets/male.png';
import female from '../../assets/female.png';
import footer from '../../assets/footer.png';
import GenderChart from '../../components/GenderChart';
import Interactive from '../InteractivePage';
import appTitle from '../../assets/app.png';
import entrantTitle from '../../assets/entrant.png';
import rateTitle from '../../assets/rate.png';
import overviewTitle from '../../assets/overview.png';


import GlobBarBallChart from '../../components/Bar3D';
import Table from '../../components/Table';




export default function HomePage(props) {
    const [reigionMap, setReigionMap] = useState([]);
    const [year, setYear] = useState("2015");
    const [yearGender, setYearGender] = useState("2015");
    const [showGlob, setShowGlob] = useState(false);
    const gender_data = {};
    Object.keys(genderData.GENDER_APP).map((item) => {
        const sum = genderData.GENDER_APP[item].reduce((accumulator, currentValue) => accumulator[1] + currentValue[1]);
        const female = genderData.GENDER_APP[item][0][1];
        gender_data[item] = {
            malePercent: Math.round((female/sum * 100)),
            femalePercent: Math.round(100 - (female/sum * 100))
        }
    });
    const overviewData = data.YEAR_OVERVIEW[year];
    function toogleView() {
        setShowGlob(!showGlob)
    }
        return (
            <div className={style.Homepage}>
                <section className={style.Overview}>
                    <div className={style.GlobeBg}>
                        <Globe id="c" texture={baseTexturePicE} />
                    </div>
                    {/* <div className={style.Content}>
                        <img src={homepage_img} alt=""/>
                    </div> */}
                </section>
                {/* <div className={style.button}>
                        <Button type="primary"
                            onClick={() => {
                                props.history.push('/interactive')
                            }}
                        >Get Start</Button>
                </div> */}
                <h2 className={style.overViewsec}>Overview information from 2015 to 2018</h2>
                <Table />
                <div className={style.yearData}>
                        <div>
                            <div className={style.barchart}>
                                <BarChart 
                                data={
                                    {
                                        "2015" : 2772,
                                        "2016" : 3916,
                                        "2017" : 6079,
                                        "2018" : 4438
                                    }
                                }
                                id="app_bar"
                                color="#3F69F0"
                                />
                            </div>
                            <img src={appTitle} alt=""/>

                            {/* <p>Apps Trend for each year.</p> */}
                        </div>
                        <div>
                            <img src={entrantTitle} alt=""/>
                            <div className={style.barchart}>
                            <BarChart 
                            data={
                                {
                                    "2015" : 979,
                                    "2016" : 1205,
                                    "2017" : 1436,
                                    "2018" : 1463
                                }
                            }
                            id="entrant_bar"
                            color="#170F65"
                            />
                            </div>
                            
                            {/* <p>Entrants Trend for each year.</p> */}
                        </div>
                        {/* <div>
                            <BarChart 
                            data={
                                {
                                    "2015" : 0.35,
                                    "2016" : 0.31,
                                    "2017" : 0.24,
                                    "2018" : 0.33
                                }
                            }
                            id="rate_bar"
                            color="#07C7EE"
                            />
                            <p>Entrants/Apps Rate Trend for each year.</p>
                        </div> */}
                    </div>
                <Interactive />
                <section className={style.introduction}>
                    <h2>Geo distribution of Applicants from 2015 to 2018</h2>
                    <div className={style.rows}>
                    <BigGlobe 
                        id="b" 
                        texture={baseTexturePicC}
                        dataIndex="2015"
                        buttonName="2015/06"
                        onChange={() => {setYear("2015")}}
                        selectedIndex={year}
                     />
                    <BigGlobe 
                        id="e" 
                        texture={baseTexturePicC}
                        dataIndex="2016"
                        buttonName="2016/07"
                        onChange={() => {setYear("2016")}}
                        selectedIndex={year}
                     />
                    <BigGlobe 
                        id="f" 
                        texture={baseTexturePicC} 
                        dataIndex="2017"
                        buttonName="2017/08"
                        onChange={() => {setYear("2017")}}
                        selectedIndex={year}
                    />
                    <BigGlobe 
                        id="g" 
                        texture={baseTexturePicC}
                        dataIndex="2018"
                        buttonName="2018/09"
                        onChange={() => {
                            setYear("2018");
                        }}
                        selectedIndex={year}
                     />
                    </div>
                    <p className={style.tips}>
                        <img src={up} alt=""/>
                        <span>Click different button to show seperate year.</span>
                    </p>
                    <div className={style.rows}>
                        <div className={style.left}>
                            {
                                showGlob ? <GlobBarBallChart selectedIndex={year} />
                                        : <GlobBarChart selectedIndex={year} max={overviewData.total_apps}/>
                            }
                        </div>
                        <div className={style.right}>
                            <Button onClick={toogleView}>Toogle View</Button>
                        <h2>Entry Year: { overviewData.name }</h2>
                            <ul>
                                <li>
                                    <h3>Highest Country: </h3>
                                    <p>{ overviewData.max_country }</p>
                                </li>
                                <li>
                                    <h3>Total Apps: </h3>
                                    <p><CountUp end={overviewData.total_apps} duration={3} delay={1}/></p>
                                </li>
                                <li>
                                    <h3>Total Entrant: </h3>
                                    <p><CountUp end={overviewData.total_entrants} duration={3} delay={1} /></p>
                                </li>
                                <li>
                                    <h3>Entrant Rate: </h3>
                                    <p><CountUp end={overviewData.app_entrants_rate} suffix="%" duration={3} delay={1} /></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {/* <h2>Region / Apps Radar</h2> */}
                    
 
                    <h2 className={style.gender}>Gender and Age Group information from 2015 to 2018</h2>
                    <div className={style.layerPannel}>
                        <div className={style.operationBar}>
                            <Button 
                                type={yearGender == '2015' ? "primary" : ''}
                                onClick={() => { setYearGender('2015') }}
                            >
                                2015/06
                            </Button>
                            <Button 
                                type={yearGender == '2016' ? "primary" : ''}
                                onClick={() => { setYearGender('2016') }}
                            >
                                2016/07
                            </Button>
                            <Button 
                                type={yearGender == '2017' ? "primary" : ''}
                                onClick={() => { setYearGender('2017') }}
                            >
                                2017/08
                            </Button>
                            <Button 
                                type={yearGender == '2018' ? "primary" : ''}
                                onClick={() => { setYearGender('2018') }}
                            >
                                2018/09
                            </Button>
                        </div>
                        <GenderChart 
                            malePercent = {gender_data[yearGender]['malePercent']}
                            femalePercent = {gender_data[yearGender]['femalePercent']}
                        />
                        <div className={style.AgeGroup}>
                            <img src={female} alt=""/>
                            <div className={style.chart}>
                            <RoseChart id={yearGender} year={yearGender}/>
                            </div>
                            <img src={male} alt=""/>
                        </div>
                        <p className={style.tips}>
                            <img src={up} alt=""/>
                            <span>Click different spot to show seperate age group.</span>
                        </p>
                    </div>
                     <img src={rateTitle} alt="" className={style.rateTitle}/>
                    {/* <SunBrust /> */}
                    <div className={style.radarChart}>
                    <Radar />
                    </div>
                    <p className={style.tips}>
                        <img src={up} alt=""/>
                        <span>Click different spot to show seperate year.</span>
                    </p>
                    
                    <div className={style.wrapper}>
                        <div className={style.bg}>
                            <img src={bgImg} alt=""/>
                        </div>
                        <div className={style.footer}>
                            <img src={footer} alt=""/>
                        </div>
                    </div>
                </section>
            </div>
        )
}