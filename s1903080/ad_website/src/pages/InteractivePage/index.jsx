import React, { useState } from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import data from '../../data/reigion_app';
import style from './style.module.scss';
import logo from '../../assets/logo.png';
import ListItem from '../../components/ListItem';



export default function Interactive(props) {
    const [ selectedYear, setSelectedYear ] = useState('2015');
    const [ selectedFact, setSelectedFact ] = useState(0);
    const [ selectedProcess, setSelectedProcess ] = useState(0);
    let selectedNation = true;

    const country_app = data.COUNTRY_APP;
    const country_entrants = data.COUNTRY_ENTRANTS;
    const region_app = data.REIGION_APP;
    const region_entrants = data.REIGION_ENTRANTS;
    const programme_app = data.PROGRAMME_APP;
    const programme_entrants = data.PROGRAMME_ENTRANTS;
    const factorList = [
        "Country",
        "Region",
        "Programme"
    ];
    const processList = ['Apps', 'Entrants', 'Entrants/Apps Rate'];
    let resData = {};
    switch (selectedFact) {
        case 0:
            switch (selectedProcess) {
                case 0:
                    resData = country_app;
                    break;
                case 1:
                    resData = country_entrants;
                    break;
                default:
                    break;
            }
            selectedNation = true;
            break;
        case 1:
            switch (selectedProcess) {
                case 0:
                    resData = region_app;
                    break;
                case 1:
                    resData = region_entrants;
                    break;
                default:
                    break;
            }
            selectedNation = false;
            break;
        case 2:
            switch (selectedProcess) {
                case 0:
                    resData = programme_app;
                    break;
                case 1:
                    resData = programme_entrants;
                    break;
                default:
                    break;
            }
            selectedNation = false;
            break;
        default:
            break;
    }
    function handleYearChange(e) {
        setSelectedYear(e.target.value);
    }
    function handleFactChange(e) {
        setSelectedFact(e.target.value);
    }
    function handleProcessChange(e) {
        setSelectedProcess(e.target.value);
    }
        return (
            <div className={style.Interactive}>
                <div className={style.pannel}>
                    <div className={style.selectPannel}>
                        <h1>Select Entry Year and Other Columns to Show Results</h1>
                        <div className={style.operationBar}>
                        <FormControl className={style.yearSelect}>
                            <InputLabel id="year">Entry Year</InputLabel>
                            <Select
                            labelId="year"
                            // id="demo-simple-select"
                            value={selectedYear}
                            onChange={handleYearChange}
                            >
                            <MenuItem value={"2015"}>2015/6</MenuItem>
                            <MenuItem value={"2016"}>2016/7</MenuItem>
                            <MenuItem value={"2017"}>2017/8</MenuItem>
                            <MenuItem value={"2018"}>2018/9</MenuItem>

                            </Select>
                        </FormControl>
                        <FormControl className={style.yearSelect}>
                            <InputLabel id="factory">Columns</InputLabel>
                            <Select
                            labelId="factory"
                            // id="demo-simple-select"
                            value={selectedFact}
                            onChange={handleFactChange}
                            >
                            <MenuItem value={0}>Country</MenuItem>
                            <MenuItem value={1}>Region</MenuItem>
                            <MenuItem value={2}>Programme</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl className={style.yearSelect}>
                            <InputLabel id="progress">Progress</InputLabel>
                            <Select
                            labelId="progress"
                            // id="demo-simple-select"
                            value={selectedProcess}
                            onChange={handleProcessChange}
                            >
                            <MenuItem value={0}>Apps</MenuItem>
                            <MenuItem value={1}>Entrants</MenuItem>
                            {/* <MenuItem value={2}>Entrants/Apps Rate</MenuItem> */}
                            </Select>
                        </FormControl>
                        </div>
                        <img src={logo} alt="logo" className={style.logo}/>
                    </div>
                <div className={style.resultPage}>
                <h2> { selectedYear }: Top {resData[selectedYear].length} { factorList[selectedFact] } of { processList[selectedProcess] }</h2>
                <ul className={style.rankList}>
                    {
                        resData[selectedYear].map((item, index) => {
                            return <li key={item}>
                                    <ListItem 
                                    rank={index+1} 
                                    name={item[0]} 
                                    value={item[1]} 
                                    selectedYear={selectedYear}
                                    selectedProcess={selectedProcess}
                                    nation={selectedNation}
                                    />
                                    </li>
                        })
                    }
                </ul>
                </div>
                </div>
                {/* <button 
                            className={style.backToHome}
                            onClick={() => {
                                props.history.push('/home')
                            }}
                        >Back to Home</button> */}
            </div>
        )
}