> Total amount of Applications for each year:

2015/6 : 2772
2016/7 : 3916
2017/8 : 6079
2018/9 : 4438
> Total amount of Offers for each year:

2015/6 : 1778
2016/7 : 2281
2017/8 : 3305
2018/9 : 2650
> Total amount of Accepts for each year:

2015/6 : 1464
2016/7 : 1783
2017/8 : 2647
2018/9 : 2174
> Total amount of UF for each year:

2015/6 : 1237
2016/7 : 1441
2017/8 : 1789
2018/9 : 1763
> Total amount of Entrants for each year:

2015/6 : 979
2016/7 : 1205
2017/8 : 1436
2018/9 : 1463

> The number of apps for each year according to school and college

{
    "2015": {
        "HSS": {
            "Law": 322,
            "ECA": 38,
            "HCA": 115,
            "PPLS": 72,
            "LLC": 64,
            "Health": 111,
            "SPS": 77,
            "Education": 167
        },
        "MVM": {
            "Dick Vet": 180,
            "Medical Sch": 104,
            "Biomed Sci": 376,
            "Clinical Sci": 672,
            "Molecular Sci": 357
        },
        "SCE": {
            "Chemistry": 7,
            "Biology": 42,
            "SCE": 23,
            "Geosciences": 45
        }
    },
    "2016": {
        "HSS": {
            "Law": 395,
            "ECA": 64,
            "HCA": 99,
            "PPLS": 73,
            "LLC": 98,
            "Health": 107,
            "SPS": 171,
            "Education": 227
        },
        "MVM": {
            "Dick Vet": 343,
            "Pop Health Sci": 1,
            "Medical Sch": 134,
            "Biomed Sci": 501,
            "Clinical Sci": 762,
            "Molecular Sci": 650
        },
        "SCE": {
            "Chemistry": 22,
            "Biology": 59,
            "SCE": 74,
            "Geosciences": 136
        }
    },
    "2017": {
        "HSS": {
            "Law": 463,
            "Business": 63,
            "ECA": 92,
            "HCA": 129,
            "PPLS": 100,
            "LLC": 64,
            "Health": 130,
            "SPS": 244,
            "Education": 144
        },
        "MVM": {
            "Dick Vet": 397,
            "Medical Sch": 271,
            "Biomed Sci": 745,
            "Clinical Sci": 971,
            "Molecular Sci": 1760
        },
        "SCE": {
            "Chemistry": 3,
            "Biology": 51,
            "SCE": 183,
            "Geosciences": 269
        }
    },
    "2018": {
        "HSS": {
            "Law": 409,
            "Business": 7,
            "ECA": 75,
            "HCA": 109,
            "PPLS": 122,
            "LLC": 2,
            "Health": 118,
            "SPS": 373,
            "Education": 133
        },
        "MVM": {
            "Dick Vet": 471,
            "Medical Sch": 172,
            "Biomed Sci": 469,
            "Clinical Sci": 767,
            "Molecular Sci": 593
        },
        "SCE": {
            "Biology": 39,
            "SCE": 178,
            "Geosciences": 401
        }
    }
}
> The number of entrants for each year according to school and college

{
    "2015": {
        "HSS": {
            "Law": 69,
            "ECA": 22,
            "HCA": 26,
            "PPLS": 34,
            "LLC": 25,
            "Health": 22,
            "SPS": 7,
            "Education": 26
        },
        "MVM": {
            "Dick Vet": 89,
            "Medical Sch": 55,
            "Biomed Sci": 97,
            "Clinical Sci": 365,
            "Molecular Sci": 112
        },
        "SCE": {
            "Chemistry": 0,
            "Biology": 14,
            "SCE": 0,
            "Geosciences": 16
        }
    },
    "2016": {
        "HSS": {
            "Law": 95,
            "ECA": 29,
            "HCA": 29,
            "PPLS": 22,
            "LLC": 31,
            "Health": 34,
            "SPS": 33,
            "Education": 50
        },
        "MVM": {
            "Dick Vet": 167,
            "Pop Health Sci": 2,
            "Medical Sch": 79,
            "Biomed Sci": 118,
            "Clinical Sci": 334,
            "Molecular Sci": 121
        },
        "SCE": {
            "Chemistry": 3,
            "Biology": 3,
            "SCE": 32,
            "Geosciences": 23
        }
    },
    "2017": {
        "HSS": {
            "Law": 89,
            "Business": 0,
            "ECA": 38,
            "HCA": 34,
            "PPLS": 43,
            "LLC": 0,
            "Health": 41,
            "SPS": 24,
            "Education": 56
        },
        "MVM": {
            "Dick Vet": 207,
            "Medical Sch": 83,
            "Biomed Sci": 139,
            "Clinical Sci": 399,
            "Molecular Sci": 176
        },
        "SCE": {
            "Chemistry": 0,
            "Biology": 4,
            "SCE": 61,
            "Geosciences": 42
        }
    },
    "2018": {
        "HSS": {
            "Law": 89,
            "Business": 0,
            "ECA": 24,
            "HCA": 34,
            "PPLS": 52,
            "LLC": 0,
            "Health": 53,
            "SPS": 55,
            "Education": 51
        },
        "MVM": {
            "Dick Vet": 219,
            "Medical Sch": 97,
            "Biomed Sci": 145,
            "Clinical Sci": 373,
            "Molecular Sci": 164
        },
        "SCE": {
            "Biology": 4,
            "SCE": 64,
            "Geosciences": 39
        }
    }
}

> The entrant/app rate for each year according to school and college

{
    "2015": {
        "HSS": {
            "Law": 0.21,
            "ECA": 0.58,
            "HCA": 0.23,
            "PPLS": 0.47,
            "LLC": 0.39,
            "Health": 0.2,
            "SPS": 0.09,
            "Education": 0.16
        },
        "MVM": {
            "Dick Vet": 0.49,
            "Medical Sch": 0.53,
            "Biomed Sci": 0.26,
            "Clinical Sci": 0.54,
            "Molecular Sci": 0.31
        },
        "SCE": {
            "Chemistry": 0.0,
            "Biology": 0.33,
            "SCE": 0.0,
            "Geosciences": 0.36
        }
    },
    "2016": {
        "HSS": {
            "Law": 0.24,
            "ECA": 0.45,
            "HCA": 0.29,
            "PPLS": 0.3,
            "LLC": 0.32,
            "Health": 0.32,
            "SPS": 0.19,
            "Education": 0.22
        },
        "MVM": {
            "Dick Vet": 0.49,
            "Pop Health Sci": 2.0,
            "Medical Sch": 0.59,
            "Biomed Sci": 0.24,
            "Clinical Sci": 0.44,
            "Molecular Sci": 0.19
        },
        "SCE": {
            "Chemistry": 0.14,
            "Biology": 0.05,
            "SCE": 0.43,
            "Geosciences": 0.17
        }
    },
    "2017": {
        "HSS": {
            "Law": 0.19,
            "Business": 0.0,
            "ECA": 0.41,
            "HCA": 0.26,
            "PPLS": 0.43,
            "LLC": 0.0,
            "Health": 0.32,
            "SPS": 0.1,
            "Education": 0.39
        },
        "MVM": {
            "Dick Vet": 0.52,
            "Medical Sch": 0.31,
            "Biomed Sci": 0.19,
            "Clinical Sci": 0.41,
            "Molecular Sci": 0.1
        },
        "SCE": {
            "Chemistry": 0.0,
            "Biology": 0.08,
            "SCE": 0.33,
            "Geosciences": 0.16
        }
    },
    "2018": {
        "HSS": {
            "Law": 0.22,
            "Business": 0.0,
            "ECA": 0.32,
            "HCA": 0.31,
            "PPLS": 0.43,
            "LLC": 0.0,
            "Health": 0.45,
            "SPS": 0.15,
            "Education": 0.38
        },
        "MVM": {
            "Dick Vet": 0.46,
            "Medical Sch": 0.56,
            "Biomed Sci": 0.31,
            "Clinical Sci": 0.49,
            "Molecular Sci": 0.28
        },
        "SCE": {
            "Biology": 0.1,
            "SCE": 0.36,
            "Geosciences": 0.1
        }
    }
}

> The number of entrants for each year according to school and Programme group.

{
    "2015": {
        "Law": {
            "International Commercial Law and Practice": 15,
            "Innovation, Technology and the Law": 4,
            "Intellectual Property Law": 2,
            "Information Technology Law": 3,
            "Law (PGT)": 36,
            "Medical Law and Ethics": 9
        },
        "Chemistry": {
            "Computational Chemistry and Modelling": 0
        },
        "Dick Vet": {
            "ProfDev Royal College of Vet Surgeons": 13,
            "International Animal Welfare, Ethics & Law": 30,
            "One Health": 10,
            "Equine Science": 23,
            "Conservation Medicine": 13
        },
        "Biology": {
            "Drug Discovery": 9,
            "Next Generation Drug Discovery": 5
        },
        "Medical Sch": {
            "Clinical Education (PGT)": 55
        },
        "Biomed Sci": {
            "Global Health Studies": 1,
            "Biodiversity, Wildlife and Ecosystem Health": 27,
            "Clinical Microbiology and Infectious Diseases": 9,
            "Global Health and Infectious Diseases": 21,
            "International Animal Health": 12,
            "Science Communication and Public Engagement": 15,
            "Anatomical Sciences": 12
        },
        "Clinical Sci": {
            "Primary Dental Care": 14,
            "Imaging": 15,
            "Clinical Ophthalmology": 12,
            "General Surgery": 22,
            "Trauma and Orthopaedics": 19,
            "Urology": 19,
            "Vascular and Endovascular Surgery": 3,
            "Clinical Management of Pain": 26,
            "Internal Medicine": 52,
            "Neuroimaging for Research": 9,
            "Primary Care Ophthalmology (Online Distance Learni": 17,
            "Paediatric Emergency Medicine": 25,
            "Surgical Sciences": 132,
            "Clinical Management of Headache Disorders": 0
        },
        "Molecular Sci": {
            "Global eHealth": 16,
            "MFM Family Medicine": 29,
            "Public Health": 27,
            "Clinical Trials": 37,
            "Global Health Challenges": 3,
            "Global Health: Non Communicable Diseases": 0
        },
        "SCE": {
            "Data Science, Technology and Innovation": 0
        },
        "ECA": {
            "Architectural Project Management": 14,
            "Digital Media Design": 8
        },
        "HCA": {
            "History (PGT)": 26
        },
        "PPLS": {
            "Epistemology, Ethics and Mind": 34
        },
        "LLC": {
            "Creative Writing (PGT)": 20,
            "Scottish Culture and Heritage": 5
        },
        "Health": {
            "Dementia: International, Policy and Practice": 4,
            "Mental Health and Well-being of Children, Young P": 18,
            "Advanced Clinical Skills": 0
        },
        "SPS": {
            "Africa and International Development": 3,
            "Global Development Challenges": 4,
            "Global Challenges": 0,
            "Global Health Policy (PGT)": 0,
            "Making Use of Digital Research": 0
        },
        "Education": {
            "Digital Education": 26,
            "Higher Education": 0,
            "Social Justice and Community Action": 0,
            "Sports Coaching and Performance": 0
        },
        "Geosciences": {
            "Carbon Management": 3,
            "Climate Change Management": 9,
            "Global Environmental Challenges": 4
        }
    },
    "2016": {
        "Law": {
            "International Commercial Law and Practice": 14,
            "Innovation, Technology and the Law": 9,
            "Intellectual Property Law": 6,
            "Information Technology Law": 5,
            "Law (PGT)": 29,
            "Medical Law and Ethics": 32
        },
        "Chemistry": {
            "Computational Chemistry and Modelling": 3
        },
        "Dick Vet": {
            "Equine Science": 25,
            "International Animal Welfare, Ethics & Law": 40,
            "Veterinary Epidemiology": 2,
            "ProfDev Royal College of Vet Surgeons": 22,
            "One Health": 12,
            "Conservation Medicine": 18,
            "Clinical Animal Behaviour": 27,
            "Disease in Livestock Ecosystems: Dynamic": 1,
            "Veterinary Anaesthesia and Analgesia": 10,
            "Advanced Clinical Practice": 10
        },
        "Biology": {
            "Drug Discovery": 3,
            "Next Generation Drug Discovery": 0
        },
        "Pop Health Sci": {
            "Public Health": 2
        },
        "Medical Sch": {
            "Clinical Education (PGT)": 79
        },
        "Biomed Sci": {
            "Global Health and Infectious Diseases": 27,
            "International Animal Health": 12,
            "Clinical Microbiology and Infectious Diseases": 17,
            "Biodiversity, Wildlife and Ecosystem Health": 35,
            "Science Communication and Public Engagement": 16,
            "Anatomical Sciences": 11,
            "Global Health Studies": 0
        },
        "Clinical Sci": {
            "Clinical Management of Pain": 26,
            "Neuroimaging for Research": 11,
            "Primary Dental Care": 14,
            "Clinical Ophthalmology": 13,
            "General Surgery": 32,
            "Trauma and Orthopaedics": 16,
            "Urology": 6,
            "Vascular and Endovascular Surgery": 11,
            "Imaging": 9,
            "Internal Medicine": 65,
            "Primary Care Ophthalmology (Online Distance Learni": 11,
            "Paediatric Emergency Medicine": 21,
            "Surgical Sciences": 99
        },
        "Molecular Sci": {
            "Public Health": 46,
            "Global eHealth": 14,
            "Global Health Challenges": 7,
            "MFM Family Medicine": 24,
            "Clinical Trials": 30
        },
        "SCE": {
            "Data Science, Technology and Innovation": 32
        },
        "ECA": {
            "Architectural Project Management": 11,
            "Digital Media Design": 18
        },
        "HCA": {
            "History (PGT)": 29
        },
        "PPLS": {
            "Epistemology, Ethics and Mind": 22
        },
        "LLC": {
            "Creative Writing (PGT)": 30,
            "Scottish Culture and Heritage": 1
        },
        "Health": {
            "Children and Young People's MH and PP": 34,
            "Dementia: International, Policy and Practice": 0,
            "Mental Health and Well-being of Children, Young P": 0
        },
        "SPS": {
            "Global Challenges": 5,
            "Africa and International Development": 10,
            "Global Development Challenges": 14,
            "Global Health Policy (PGT)": 4,
            "Making Use of Digital Research": 0
        },
        "Education": {
            "Digital Education": 50,
            "Social Justice and Community Action": 0,
            "Sports Coaching and Performance": 0
        },
        "Geosciences": {
            "Carbon Management": 15,
            "Carbon Innovation": 2,
            "Climate Change Management": 2,
            "Global Environmental Challenges": 4
        }
    },
    "2017": {
        "Law": {
            "International Commercial Law and Practice": 19,
            "Innovation, Technology and the Law": 17,
            "Intellectual Property Law": 6,
            "Information Technology Law": 8,
            "Law (PGT)": 22,
            "Medical Law and Ethics": 17
        },
        "Chemistry": {
            "Computational Chemistry and Modelling": 0
        },
        "Dick Vet": {
            "Equine Science": 43,
            "International Animal Welfare, Ethics & Law": 47,
            "ProfDev Royal College of Vet Surgeons": 23,
            "Veterinary Epidemiology": 5,
            "Clinical Animal Behaviour": 35,
            "One Health": 9,
            "Veterinary Anaesthesia and Analgesia": 12,
            "Advanced Clinical Practice": 15,
            "Conservation Medicine": 18
        },
        "Biology": {
            "Drug Discovery": 4,
            "Next Generation Drug Discovery": 0
        },
        "Medical Sch": {
            "Clinical Education": 1,
            "Clinical Education (PGT)": 82
        },
        "Biomed Sci": {
            "Clinical Microbiology and Infectious Diseases": 15,
            "Global Health and Infectious Diseases": 31,
            "International Animal Health": 8,
            "Anatomical Sciences": 20,
            "Biodiversity, Wildlife and Ecosystem Health": 42,
            "Science Communication and Public Engagement": 23
        },
        "Clinical Sci": {
            "Neuroimaging for Research": 19,
            "Clinical Management of Pain": 32,
            "Imaging": 20,
            "Primary Dental Care": 11,
            "Clinical Ophthalmology": 18,
            "General Surgery": 24,
            "Trauma and Orthopaedics": 25,
            "Urology": 13,
            "Vascular and Endovascular Surgery": 7,
            "Internal Medicine": 81,
            "Primary Care Ophthalmology (Online Distance Learni": 10,
            "Paediatric Emergency Medicine": 35,
            "Stem Cells and Translational Neurology": 7,
            "Surgical Sciences": 97
        },
        "Molecular Sci": {
            "Global eHealth": 11,
            "Molecular Pathology and Genomic Medicine": 14,
            "MFM Family Medicine": 41,
            "Public Health": 60,
            "Clinical Trials": 41,
            "Global Health Challenges": 7,
            "Data Science, Technology and Innovation": 2
        },
        "SCE": {
            "Data Science, Technology and Innovation": 61
        },
        "Business": {
            "Accounting and Society": 0
        },
        "ECA": {
            "Architectural Project Management": 10,
            "Digital Media Design": 28
        },
        "HCA": {
            "History (PGT)": 34
        },
        "PPLS": {
            "Epistemology, Ethics and Mind": 25,
            "Philosophy (PGR)": 1,
            "Philosophy, Science and Religion": 17
        },
        "LLC": {
            "Creative Writing (PGT)": 0,
            "Scottish Culture and Heritage": 0
        },
        "Health": {
            "Children and Young People's MH and PP": 41,
            "Dementia: International, Policy and Practice": 0
        },
        "SPS": {
            "Global Challenges": 1,
            "International Development (PGT)": 5,
            "Africa and International Development": 6,
            "Global Development Challenges": 9,
            "Global Health Policy (PGT)": 3,
            "Making Use of Digital Research": 0
        },
        "Education": {
            "Digital Education": 56,
            "Sports Coaching and Performance": 0
        },
        "Geosciences": {
            "Carbon Management": 26,
            "Climate Change Management": 14,
            "Global Environmental Challenges": 2,
            "Carbon Innovation": 0
        }
    },
    "2018": {
        "Law": {
            "International Commercial Law and Practice": 13,
            "Innovation, Technology and the Law": 12,
            "Intellectual Property Law": 4,
            "Information Technology Law": 13,
            "Medical Law and Ethics": 34,
            "Law (PGT)": 13
        },
        "Dick Vet": {
            "Equine Science": 24,
            "International Animal Welfare, Ethics & Law": 39,
            "Applied Poultry Science": 9,
            "ProfDev Royal College of Vet Surgeons": 9,
            "Applied Conservation Genetics with Wildlife Forensics": 8,
            "Clinical Animal Behaviour": 51,
            "Food Safety": 3,
            "One Health": 20,
            "Veterinary Anaesthesia and Analgesia": 11,
            "Veterinary Epidemiology": 5,
            "Advanced Clinical Practice": 13,
            "Conservation Medicine": 26,
            "Global Food Security and Nutrition": 1
        },
        "Biology": {
            "Drug Discovery": 1,
            "Next Generation Drug Discovery": 3
        },
        "Medical Sch": {
            "Clinical Education (PGT)": 94,
            "Clinical Education": 3
        },
        "Biomed Sci": {
            "Clinical Microbiology and Infectious Diseases": 23,
            "Global Health and Infectious Diseases": 23,
            "Anatomical Sciences": 10,
            "Biodiversity, Wildlife and Ecosystem Health": 58,
            "International Animal Health": 6,
            "Science Communication and Public Engagement": 25,
            "Global Health Studies": 0,
            "Biodiversity Wildlife and Ecosystem Health": 0
        },
        "Clinical Sci": {
            "Clinical Management of Pain": 21,
            "Imaging": 15,
            "PET-MR Principles and Applications": 1,
            "Clinical Ophthalmology": 17,
            "General Surgery": 29,
            "Trauma and Orthopaedics": 23,
            "Urology": 16,
            "Vascular and Endovascular Surgery": 12,
            "Internal Medicine": 58,
            "Neuroimaging for Research": 5,
            "Primary Care Ophthalmology (Online Distance Learni": 9,
            "Paediatric Emergency Medicine": 23,
            "Patient Safety and Clinical Human Factors": 23,
            "Stem Cells and Translational Neurology": 14,
            "Surgical Sciences": 107,
            "Primary Dental Care": 0
        },
        "Molecular Sci": {
            "Clinical Trials": 42,
            "MFM Family Medicine": 34,
            "Public Health": 58,
            "Global eHealth": 7,
            "Molecular Pathology and Genomic Medicine": 20,
            "Global Health Challenges": 3
        },
        "SCE": {
            "Data Science, Technology and Innovation": 64
        },
        "Business": {
            "Accounting and Society": 0
        },
        "ECA": {
            "Digital Media Design": 24,
            "Architectural Project Management": 0
        },
        "HCA": {
            "History (PGT)": 34
        },
        "PPLS": {
            "Epistemology, Ethics and Mind": 13,
            "Philosophy (PGR)": 1,
            "Philosophy, Science and Religion": 38
        },
        "LLC": {
            "Scottish Culture and Heritage": 0
        },
        "Health": {
            "School of Health in Social Science": 4,
            "Children and Young People's MH and PP": 49
        },
        "SPS": {
            "Global Health Policy": 5,
            "Global Challenges": 2,
            "International Development (PGT)": 44,
            "Africa and International Development": 1,
            "Global Development Challenges": 3,
            "Global Health Policy (PGT)": 0,
            "Making Use of Digital Research": 0
        },
        "Education": {
            "Digital Education": 51
        },
        "Geosciences": {
            "Carbon Management": 23,
            "Climate Change Management": 13,
            "Global Environmental Challenges": 3,
            "Carbon Innovation": 0
        }
    }
}

> The Programme list of each year.

{
    "2015": {
        "HSS": {
            "Law": ["International Commercial Law and Practice",
                "Innovation, Technology and the Law",
                "Intellectual Property Law",
                "Information Technology Law",
                "Law (PGT)",
                "Medical Law and Ethics"
            ],
            "ECA": ["Architectural Project Management", "Digital Media Design"],
            "HCA": ["History (PGT)"],
            "PPLS": ["Epistemology, Ethics and Mind"],
            "LLC": ["Creative Writing (PGT)", "Scottish Culture and Heritage"],
            "Health": ["Dementia: International, Policy and Practice",
                "Mental Health and Well-being of Children, Young P",
                "Advanced Clinical Skills"
            ],
            "SPS": ["Africa and International Development",
                "Global Development Challenges",
                "Global Challenges",
                "Global Health Policy (PGT)",
                "Making Use of Digital Research"
            ],
            "Education": ["Digital Education",
                "Higher Education",
                "Social Justice and Community Action",
                "Sports Coaching and Performance"
            ]
        },
        "SCE": {
            "Chemistry": ["Computational Chemistry and Modelling"],
            "Biology": ["Drug Discovery", "Next Generation Drug Discovery"],
            "SCE": ["Data Science, Technology and Innovation"],
            "Geosciences": ["Carbon Management",
                "Climate Change Management",
                "Global Environmental Challenges"
            ]
        },
        "MVM": {
            "Dick Vet": ["ProfDev Royal College of Vet Surgeons",
                "International Animal Welfare, Ethics & Law",
                "One Health",
                "Equine Science",
                "Conservation Medicine"
            ],
            "Medical Sch": ["Clinical Education (PGT)"],
            "Biomed Sci": ["Global Health Studies",
                "Biodiversity, Wildlife and Ecosystem Health",
                "Clinical Microbiology and Infectious Diseases",
                "Global Health and Infectious Diseases",
                "International Animal Health",
                "Science Communication and Public Engagement",
                "Anatomical Sciences"
            ],
            "Clinical Sci": ["Primary Dental Care",
                "Imaging",
                "Clinical Ophthalmology",
                "General Surgery",
                "Trauma and Orthopaedics",
                "Urology",
                "Vascular and Endovascular Surgery",
                "Clinical Management of Pain",
                "Internal Medicine",
                "Neuroimaging for Research",
                "Primary Care Ophthalmology (Online Distance Learni",
                "Paediatric Emergency Medicine",
                "Surgical Sciences",
                "Clinical Management of Headache Disorders"
            ],
            "Molecular Sci": ["Global eHealth",
                "MFM Family Medicine",
                "Public Health",
                "Clinical Trials",
                "Global Health Challenges",
                "Global Health: Non Communicable Diseases"
            ]
        }
    },
    "2016": {
        "HSS": {
            "Law": ["International Commercial Law and Practice",
                "Innovation, Technology and the Law",
                "Intellectual Property Law",
                "Information Technology Law",
                "Law (PGT)",
                "Medical Law and Ethics"
            ],
            "ECA": ["Architectural Project Management", "Digital Media Design"],
            "HCA": ["History (PGT)"],
            "PPLS": ["Epistemology, Ethics and Mind"],
            "LLC": ["Creative Writing (PGT)", "Scottish Culture and Heritage"],
            "Health": ["Children and Young People's MH and PP",
                "Dementia: International, Policy and Practice",
                "Mental Health and Well-being of Children, Young P"
            ],
            "SPS": ["Global Challenges",
                "Africa and International Development",
                "Global Development Challenges",
                "Global Health Policy (PGT)",
                "Making Use of Digital Research"
            ],
            "Education": ["Digital Education",
                "Social Justice and Community Action",
                "Sports Coaching and Performance"
            ]
        },
        "SCE": {
            "Chemistry": ["Computational Chemistry and Modelling"],
            "Biology": ["Drug Discovery", "Next Generation Drug Discovery"],
            "SCE": ["Data Science, Technology and Innovation"],
            "Geosciences": ["Carbon Management",
                "Carbon Innovation",
                "Climate Change Management",
                "Global Environmental Challenges"
            ]
        },
        "MVM": {
            "Dick Vet": ["Equine Science",
                "International Animal Welfare, Ethics & Law",
                "Veterinary Epidemiology",
                "ProfDev Royal College of Vet Surgeons",
                "One Health",
                "Conservation Medicine",
                "Clinical Animal Behaviour",
                "Disease in Livestock Ecosystems: Dynamic",
                "Veterinary Anaesthesia and Analgesia",
                "Advanced Clinical Practice"
            ],
            "Pop Health Sci": ["Public Health"],
            "Medical Sch": ["Clinical Education (PGT)"],
            "Biomed Sci": ["Global Health and Infectious Diseases",
                "International Animal Health",
                "Clinical Microbiology and Infectious Diseases",
                "Biodiversity, Wildlife and Ecosystem Health",
                "Science Communication and Public Engagement",
                "Anatomical Sciences",
                "Global Health Studies"
            ],
            "Clinical Sci": ["Clinical Management of Pain",
                "Neuroimaging for Research",
                "Primary Dental Care",
                "Clinical Ophthalmology",
                "General Surgery",
                "Trauma and Orthopaedics",
                "Urology",
                "Vascular and Endovascular Surgery",
                "Imaging",
                "Internal Medicine",
                "Primary Care Ophthalmology (Online Distance Learni",
                "Paediatric Emergency Medicine",
                "Surgical Sciences"
            ],
            "Molecular Sci": ["Public Health",
                "Global eHealth",
                "Global Health Challenges",
                "MFM Family Medicine",
                "Clinical Trials"
            ]
        }
    },
    "2017": {
        "HSS": {
            "Law": ["International Commercial Law and Practice",
                "Innovation, Technology and the Law",
                "Intellectual Property Law",
                "Information Technology Law",
                "Law (PGT)",
                "Medical Law and Ethics"
            ],
            "Business": ["Accounting and Society"],
            "ECA": ["Architectural Project Management", "Digital Media Design"],
            "HCA": ["History (PGT)"],
            "PPLS": ["Epistemology, Ethics and Mind",
                "Philosophy (PGR)",
                "Philosophy, Science and Religion"
            ],
            "LLC": ["Creative Writing (PGT)", "Scottish Culture and Heritage"],
            "Health": ["Children and Young People's MH and PP",
                "Dementia: International, Policy and Practice"
            ],
            "SPS": ["Global Challenges",
                "International Development (PGT)",
                "Africa and International Development",
                "Global Development Challenges",
                "Global Health Policy (PGT)",
                "Making Use of Digital Research"
            ],
            "Education": ["Digital Education", "Sports Coaching and Performance"]
        },
        "SCE": {
            "Chemistry": ["Computational Chemistry and Modelling"],
            "Biology": ["Drug Discovery", "Next Generation Drug Discovery"],
            "SCE": ["Data Science, Technology and Innovation"],
            "Geosciences": ["Carbon Management",
                "Climate Change Management",
                "Global Environmental Challenges",
                "Carbon Innovation"
            ]
        },
        "MVM": {
            "Dick Vet": ["Equine Science",
                "International Animal Welfare, Ethics & Law",
                "ProfDev Royal College of Vet Surgeons",
                "Veterinary Epidemiology",
                "Clinical Animal Behaviour",
                "One Health",
                "Veterinary Anaesthesia and Analgesia",
                "Advanced Clinical Practice",
                "Conservation Medicine"
            ],
            "Medical Sch": ["Clinical Education", "Clinical Education (PGT)"],
            "Biomed Sci": ["Clinical Microbiology and Infectious Diseases",
                "Global Health and Infectious Diseases",
                "International Animal Health",
                "Anatomical Sciences",
                "Biodiversity, Wildlife and Ecosystem Health",
                "Science Communication and Public Engagement"
            ],
            "Clinical Sci": ["Neuroimaging for Research",
                "Clinical Management of Pain",
                "Imaging",
                "Primary Dental Care",
                "Clinical Ophthalmology",
                "General Surgery",
                "Trauma and Orthopaedics",
                "Urology",
                "Vascular and Endovascular Surgery",
                "Internal Medicine",
                "Primary Care Ophthalmology (Online Distance Learni",
                "Paediatric Emergency Medicine",
                "Stem Cells and Translational Neurology",
                "Surgical Sciences"
            ],
            "Molecular Sci": ["Global eHealth",
                "Molecular Pathology and Genomic Medicine",
                "MFM Family Medicine",
                "Public Health",
                "Clinical Trials",
                "Global Health Challenges",
                "Data Science, Technology and Innovation"
            ]
        }
    },
    "2018": {
        "HSS": {
            "Law": ["International Commercial Law and Practice",
                "Innovation, Technology and the Law",
                "Intellectual Property Law",
                "Information Technology Law",
                "Medical Law and Ethics",
                "Law (PGT)"
            ],
            "Business": ["Accounting and Society"],
            "ECA": ["Digital Media Design", "Architectural Project Management"],
            "HCA": ["History (PGT)"],
            "PPLS": ["Epistemology, Ethics and Mind",
                "Philosophy (PGR)",
                "Philosophy, Science and Religion"
            ],
            "LLC": ["Scottish Culture and Heritage"],
            "Health": ["School of Health in Social Science",
                "Children and Young People's MH and PP"
            ],
            "SPS": ["Global Health Policy",
                "Global Challenges",
                "International Development (PGT)",
                "Africa and International Development",
                "Global Development Challenges",
                "Global Health Policy (PGT)",
                "Making Use of Digital Research"
            ],
            "Education": ["Digital Education"]
        },
        "MVM": {
            "Dick Vet": ["Equine Science",
                "International Animal Welfare, Ethics & Law",
                "Applied Poultry Science",
                "ProfDev Royal College of Vet Surgeons",
                "Applied Conservation Genetics with Wildlife Forensics",
                "Clinical Animal Behaviour",
                "Food Safety",
                "One Health",
                "Veterinary Anaesthesia and Analgesia",
                "Veterinary Epidemiology",
                "Advanced Clinical Practice",
                "Conservation Medicine",
                "Global Food Security and Nutrition"
            ],
            "Medical Sch": ["Clinical Education (PGT)", "Clinical Education"],
            "Biomed Sci": ["Clinical Microbiology and Infectious Diseases",
                "Global Health and Infectious Diseases",
                "Anatomical Sciences",
                "Biodiversity, Wildlife and Ecosystem Health",
                "International Animal Health",
                "Science Communication and Public Engagement",
                "Global Health Studies",
                "Biodiversity Wildlife and Ecosystem Health"
            ],
            "Clinical Sci": ["Clinical Management of Pain",
                "Imaging",
                "PET-MR Principles and Applications",
                "Clinical Ophthalmology",
                "General Surgery",
                "Trauma and Orthopaedics",
                "Urology",
                "Vascular and Endovascular Surgery",
                "Internal Medicine",
                "Neuroimaging for Research",
                "Primary Care Ophthalmology (Online Distance Learni",
                "Paediatric Emergency Medicine",
                "Patient Safety and Clinical Human Factors",
                "Stem Cells and Translational Neurology",
                "Surgical Sciences",
                "Primary Dental Care"
            ],
            "Molecular Sci": ["Clinical Trials",
                "MFM Family Medicine",
                "Public Health",
                "Global eHealth",
                "Molecular Pathology and Genomic Medicine",
                "Global Health Challenges"
            ]
        },
        "SCE": {
            "Biology": ["Drug Discovery", "Next Generation Drug Discovery"],
            "SCE": ["Data Science, Technology and Innovation"],
            "Geosciences": ["Carbon Management",
                "Climate Change Management",
                "Global Environmental Challenges",
                "Carbon Innovation"
            ]
        }
    }
}
