{
    "2015": [
        ['England', 481],
        ['Scotland', 195],
        ['USA', 195],
        ['Australia', 141],
        ['Nigeria', 134],
        ['Uganda', 95],
        ['India', 75],
        ['Canada', 73],
        ['Pakistan', 71],
        ['Ghana', 69]
    ],
    "2016": [
        ['England', 618],
        ['Nigeria', 260],
        ['USA', 251],
        ['Scotland', 248],
        ['Kenya', 152],
        ['India', 127],
        ['Australia', 118],
        ['Ghana', 117],
        ['Uganda', 116],
        ['Canada', 107]
    ],
    "2017": [
        ['England', 759],
        ['Nigeria', 711],
        ['Ghana', 317],
        ['Uganda', 310],
        ['USA', 292],
        ['Kenya', 291],
        ['India', 266],
        ['Scotland', 249],
        ['Pakistan', 187],
        ['Tanzania', 143]
    ],
    "2018": [
        ['England', 751],
        ['USA', 301],
        ['Nigeria', 269],
        ['Scotland', 268],
        ['Kenya', 213],
        ['India', 152],
        ['Uganda', 127],
        ['Canada', 115],
        ['Australia', 111],
        ['Zimbabwe', 95]]
}