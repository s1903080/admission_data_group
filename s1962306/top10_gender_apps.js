{
    "2015": [
        ['England', 248],
        ['Australia', 104],
        ['Scotland', 94],
        ['USA', 76],
        ['Canada', 31],
        ['Ireland', 23],
        ['India', 21],
        ['Nigeria', 21],
        ['Germany', 17],
        ['UAE', 15]
    ],
    "2016": [
        ['England', 295],
        ['Scotland', 147],
        ['USA', 88],
        ['Australia', 78],
        ['Canada', 56],
        ['Ireland', 39],
        ['Singapore', 32],
        ['India', 25],
        ['Germany', 20],
        ['Malta', 20]
    ],
    "2017": [
        ['England', 383],
        ['Scotland', 137],
        ['USA', 102],
        ['Australia', 92],
        ['Canada', 66],
        ['Ireland', 56],
        ['India', 33],
        ['South Africa', 31],
        ['Nigeria', 23],
        ['UAE', 23]
    ],
    "2018": [
        ['England', 388],
        ['USA', 154],
        ['Scotland', 153],
        ['Australia', 68],
        ['Canada', 51],
        ['Ireland', 46],
        ['India', 28],
        ['Kenya', 23],
        ['Malaysia', 23],
        ['Germany', 21]
        ]
}